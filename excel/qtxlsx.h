#ifndef QTXLSX_H
#define QTXLSX_H

#include <QObject>
#include <QAxObject>
#include <shlobj.h>
#include "xlsxworkbook.h"

class qtXlsx : public QAxObject
{
private:
    QAxObject *excel;
    QAxObject *books;
public:
    qtXlsx(bool visible = true);

    xlsxWorkbook *workbook;

    void openWorkbook(QString filePath);
    void closeWorkbooks();
    void setScreenUpdates(bool scr);
    void setVisible(bool visible);
};

#endif // QTXLSX_H
