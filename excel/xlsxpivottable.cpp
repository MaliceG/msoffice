#include "xlsxpivottable.h"
#include <QDebug>
#include <QTextEdit>

xlsxPivotTable::xlsxPivotTable()
{

}

QString xlsxPivotTable::getPivotName()
{
    return this->property("Name").toString();
}

bool xlsxPivotTable::setFieldOrientation(QString fieldName, xlsxFieldType fieldType)
{
    QAxObject *pivotField = this->querySubObject("PivotFields(QString)", fieldName);
    if (!pivotField) return false;
    pivotField->setProperty("Orientation", fieldType);
    pivotField->deleteLater();

    return true;
}

bool xlsxPivotTable::setFieldOrientation(QString fieldName, int fieldType)
{
    QAxObject *pivotField = this->querySubObject("PivotFields(QString)", fieldName);
    if (!pivotField) return false;
    pivotField->setProperty("Orientation", fieldType);
    pivotField->deleteLater();

    return true;
}

int xlsxPivotTable::getFieldOrientation(QString fieldName)
{
    QAxObject *pivotField = this->querySubObject("PivotFields(QString)", fieldName);
    return pivotField->property("Orientation").toInt();
}

bool xlsxPivotTable::addDataField(QString fieldName, QString caption, xlsxPivotTable::XlConsolidationFunction function)
{
    QAxObject *pivotField = this->querySubObject("PivotFields(QString)", fieldName);
    if (pivotField)
    {
        this->dynamicCall("AddDataField(QVariant, QString, XlConsolidationFunction)", pivotField->asVariant(), caption, function);
        pivotField->deleteLater();
    }
    else
        return false;

    return true;
}

void xlsxPivotTable::removeDataField(QString fieldName)
{
    for (int i = 1; i <= this->getDataFieldsCount(); i++)
        if (this->getDataField(i)->getSourceName() == fieldName)
            this->getDataField(i)->dynamicCall("Orientation", xlHidden);
}

bool xlsxPivotTable::clearFilter(QString fieldName)
{
    QAxObject *pivotField = this->querySubObject("PivotFields(QString)", fieldName);
    if (pivotField) pivotField->dynamicCall("ClearAllFilters()");
    else return false;

    return true;
}

QPoint xlsxPivotTable::getTopLeftCell()
{
    QAxObject *rowRange = this->querySubObject("RowRange");
    QPoint result;
    result.setX(rowRange->dynamicCall("Row").toInt());
    result.setY(rowRange->dynamicCall("Column").toInt());
    return result;
}

QRect xlsxPivotTable::getDataRange()
{
    QPoint startPoint = getTopLeftCell();
    QSize endPoint; //QPoint делает вычетание высоты при объединении QPoint1 & QPoint2, а QSize - нет

    QAxObject *rowRange = this->querySubObject("RowRange");
    QAxObject *columnsRange = this->querySubObject("DataBodyRange");
    QAxObject *columns = columnsRange->querySubObject("Columns");

//    qDebug() << startPoint.x();
//    qDebug() << startPoint.y();

//    qDebug() << rowRange->dynamicCall("Count").toInt();
//    qDebug() << columns->dynamicCall("Count").toInt();


    endPoint.setHeight(rowRange->dynamicCall("Count").toInt() + startPoint.x() - 1); //Т.к. StartPoint уже содержит первую ячейку, значит нужно делать смещение на -1
    endPoint.setWidth(columns->dynamicCall("Count").toInt() + startPoint.y()); //Данный метод не захватывает первую ячейку, так что не надо делать смещение

//    qDebug() << endPoint;

    return QRect(startPoint, endPoint);
}

bool xlsxPivotTable::isSaveData()
{
    return this->property("SaveData").toBool();
}

bool xlsxPivotTable::setSingleFilter(QString field, QString filter)
{
    QAxObject *pivotField = this->querySubObject("PivotFields(QString)", field);
    pivotField->dynamicCall("ClearAllFilters()");

    QAxObject *pivotItems = pivotField->querySubObject("PivotItems");
    if (!pivotItems) return false;

    int count = pivotItems->property("Count").toInt();

    for (int i = 1; i <= count; i++) //подсчет начинается с 1
    {
        QAxObject *pivotItem = pivotField->querySubObject("PivotItems(int)", i);
        if (filter != pivotItem->property("Name").toString() )
        {
            if (!pivotItems) return false;
            pivotItem->setProperty("Visible", false);
        }
    }

    return true;
}

bool xlsxPivotTable::setMultipleFilter(QString field, QStringList filters)
{
    QAxObject *pivotField = this->querySubObject("PivotFields(QString)", field);
    pivotField->dynamicCall("ClearAllFilters()");
    QAxObject *pivotItems = pivotField->querySubObject("PivotItems");
    if (!pivotItems) return false;

    int count = pivotItems->property("Count").toInt();

    for (int i = 1; i <= count; i++) //подсчет начинается с 1
    {
        QAxObject *pivotItem = pivotField->querySubObject("PivotItems(int)", i);
        if (!pivotItem) return false;
        if (!filters.contains( pivotItem->property("Name").toString() ))
            pivotItem->setProperty("Visible", false);
    }

    return true;
}

void xlsxPivotTable::setCalculationType(QString field, xlsxPivotTable::XlPivotFieldCalculation type)
{
    QAxObject *pivotField = this->querySubObject("PivotFields(QString)", field);
    pivotField->setProperty("Calculation", type);
}

void xlsxPivotTable::setSort(QString sortedField, QString sortByField, xlsxPivotTable::XlSortOrder order)
{
    QAxObject *pivotField = this->querySubObject("PivotFields(QString)", sortedField);
    pivotField->dynamicCall("AutoSort(XlSortOrder, QString)", order, sortByField);
}

void xlsxPivotTable::setTotalGrand(bool boolean)
{
    this->setProperty("ColumnGrand", boolean);
    this->setProperty("RowGrand", boolean);
}

void xlsxPivotTable::setRowGrand(bool boolean)
{
    this->setProperty("RowGrand", boolean);
}

void xlsxPivotTable::setColumnGrand(bool boolean)
{
    this->setProperty("ColumnGrand", boolean);
}

void xlsxPivotTable::setRowAxisLayout(xlsxPivotTable::XlLayoutRowType type)
{
    this->setProperty("RowAxisLayout", type);
}

void xlsxPivotTable::setSaveData(bool saveData)
{
    if (isSaveData() != saveData)
    this->setProperty("SaveData", saveData);
}

void xlsxPivotTable::clearAllFields()
{
    QAxObject *pivotFields = this->querySubObject("PivotFields");
    int count = pivotFields->property("Count").toInt();
    for (int i = 1; i <= count; i++)
        pivotFields->querySubObject("Item(int)", i)->setProperty("Orientation", xlHidden);

    QAxObject *dataFields = this->querySubObject("DataFields");
    count = dataFields->property("Count").toInt();
    for (int i = 1; i <= count; i++)
        dataFields->querySubObject("Item(int)", i)->setProperty("Orientation", xlHidden);
}

int xlsxPivotTable::getFieldPosition(QString fieldName)
{
    QAxObject *pivotField = this->querySubObject("PivotFields(QString)", fieldName);
    if (pivotField) return pivotField->property("Position").toInt();
    else return -1;
}

bool xlsxPivotTable::setFieldPosition(QString fieldName, int pos)
{
    QAxObject *pivotField = this->querySubObject("PivotFields(QString)", fieldName);
    if (pivotField)
        pivotField->setProperty("Position", pos);
    else
        return false;

    return true;
}

xlsxPivotField *xlsxPivotTable::getPivotField(int index)
{
    return (xlsxPivotField*) this->querySubObject("PivotFields")->querySubObject("Item(int)", index);
}

xlsxPivotField *xlsxPivotTable::getPivotField(QString fieldName)
{
    xlsxPivotField *pivotField = (xlsxPivotField*) this->querySubObject("PivotFields(QString)", fieldName);
    if (pivotField)
        return pivotField;
    else
        return nullptr;
}

xlsxDataField *xlsxPivotTable::getDataField(int index)
{
    return (xlsxDataField*) this->querySubObject("DataFields")->querySubObject("Item(int)", index);
}

QAxObject *xlsxPivotTable::getParent()
{
    return this->querySubObject("Parent");
}

void xlsxPivotTable::selectPivotTable()
{
    this->querySubObject("TableRange2")->dynamicCall("Select()");
}

void xlsxPivotTable::copyPivotTable()
{
    this->querySubObject("TableRange2")->dynamicCall("Copy()");
}

int xlsxPivotTable::getFieldsCount()
{
    return this->querySubObject("PivotFields")->property("Count").toInt();
}

int xlsxPivotTable::getDataFieldsCount()
{
    return this->querySubObject("DataFields")->property("Count").toInt();
}

QStringList xlsxPivotTable::getRowFields()
{
    QStringList fieldsList;
    for (int i = 1; i <= this->getFieldsCount(); i++)
        if (this->getPivotField(i)->getOrientation() == 1)
            fieldsList.append(this->getPivotField(i)->getName());
    return fieldsList;
}

QStringList xlsxPivotTable::getColumnFields()
{
    QStringList fieldsList;
    for (int i = 1; i <= this->getFieldsCount(); i++)
        if (this->getPivotField(i)->getOrientation() == 2)
            fieldsList.append(this->getPivotField(i)->getName());
    return fieldsList;
}

QStringList xlsxPivotTable::getPageFields()
{
    QStringList fieldsList;
    for (int i = 1; i <= this->getFieldsCount(); i++)
        if (this->getPivotField(i)->getOrientation() == 3)
            fieldsList.append(this->getPivotField(i)->getName());
    return fieldsList;
}

QStringList xlsxPivotTable::getDataFields()
{
    QStringList fieldsList;
    for (int i = 1; i <= this->getDataFieldsCount(); i++)
        fieldsList.append(this->getDataField(i)->getSourceName());
    return fieldsList;
}

QStringList xlsxPivotTable::getAllFields()
{
    QStringList fieldsList;
    fieldsList.append(this->getRowFields());
    fieldsList.append(this->getColumnFields());
    fieldsList.append(this->getPageFields());
    fieldsList.append(this->getDataFields());
    return fieldsList;
}

