#ifndef XLSXPIVOTTABLE_H
#define XLSXPIVOTTABLE_H

#include <QObject>
#include <QAxObject>
#include <QPoint>
#include <QRect>

class xlsxDataField : public QAxObject
{
public:
    QString getSourceName(){return this->property("SourceName").toString();}
};

class xlsxPivotField : public QAxObject
{
public:

    QString getName() {return this->property("Name").toString();}
    int getOrientation() {return this->property("Orientation").toInt();}
    int getPosition() {return this->property("Position").toInt();}
    void setPosition(int position) {this->setProperty("Position", position);}
    bool isAllItemsVisible(){return this->property("AllItemsVisible").toBool();}

    int pivotItemsCount(){return this->property("Count").toInt();}
    QString isPivotItemVisible(int index)
    {
        QAxObject *obj = this->querySubObject("PivotItems(int)", index);
        if (obj)
        {
            obj->property("Visible").toBool();
            return obj->property("Name").toString();
        }
        return QString();
    }
};

class xlsxPivotTable : public QAxObject
{
public:

    enum xlsxFieldType{
        xlHidden = 0,
        xlRowField = 1,
        xlColumnField = 2,
        xlPageField = 3,
        xlDataField = 4
    };

    enum XlConsolidationFunction{
        xlSum = -4157
    };

    enum XlPivotFieldCalculation{
        xlPercentOfColumn = 7,
        xlPercentOfRow = 6
    };

    enum XlSortOrder{
        xlAscending = 1,
        xlDescending = 2
    };

    enum XlLayoutRowType{
        xlDefault = 0,
        xlCompactRow = 0,
        xlTabularRow = 1,
        xlOutlineRow = 2
    };

    xlsxPivotTable();
    QString getPivotName();

    QPoint getTopLeftCell();
    QRect getDataRange();

    bool isSaveData();
    bool setFieldOrientation(QString fieldName, xlsxFieldType fieldType);
    bool setFieldOrientation(QString fieldName, int fieldType);
    int getFieldOrientation(QString fieldName);
    bool addDataField(QString fieldName, QString caption, XlConsolidationFunction function);
    void removeDataField(QString fieldName);
    bool clearFilter(QString fieldName);
    bool setSingleFilter(QString field, QString filter);
    bool setMultipleFilter(QString field, QStringList filters);
    void setCalculationType(QString field, XlPivotFieldCalculation type);
    void setSort(QString sortedField, QString sortByField, XlSortOrder order);
    void setTotalGrand(bool boolean);
    void setRowGrand(bool boolean);
    void setColumnGrand(bool boolean);
    void setRowAxisLayout(XlLayoutRowType type);
    void setSaveData(bool saveData);
    void clearAllFields();
    int getFieldPosition(QString fieldName);
    bool setFieldPosition(QString fieldName, int index);
    xlsxPivotField* getPivotField(int index);
    xlsxPivotField* getPivotField(QString fieldName);
    xlsxDataField *getDataField(int index);
    QAxObject *getParent();
    void selectPivotTable();
    void copyPivotTable();
    int getFieldsCount();
    int getDataFieldsCount();

    QStringList getRowFields();
    QStringList getColumnFields();
    QStringList getPageFields();
    QStringList getDataFields();
    QStringList getAllFields();
};

#endif // XLSXPIVOTTABLE_H
