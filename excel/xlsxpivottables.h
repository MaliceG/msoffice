#ifndef XLSXPIVOTTABLES_H
#define XLSXPIVOTTABLES_H

#include <QObject>
#include <QAxObject>
#include "xlsxpivottable.h"

class xlsxPivotTables : public QAxObject
{
public:
    xlsxPivotTables();

    xlsxPivotTable *activePivot;
    xlsxPivotTable *getPivot(int index);

    int getCount();
};

#endif // XLSXPIVOTTABLES_H
