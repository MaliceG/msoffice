#include "xlsxworkbook.h"
#include <QDebug>

xlsxWorkbook::xlsxWorkbook()
{

}

xlsxWorksheet *xlsxWorkbook::openSheet(int index)
{
    QAxObject *sheets = this->querySubObject("Worksheets");
    if (sheets) return (xlsxWorksheet*) sheets->querySubObject("Item(int)", index);
    else return nullptr;
}

xlsxWorksheet *xlsxWorkbook::openSheet(QString sheetName)
{
    QAxObject *sheets = this->querySubObject("Worksheets(const QString&)", sheetName);
    if (sheets) return (xlsxWorksheet*) sheets;
    else return nullptr;
}

int xlsxWorkbook::getSheetsCount()
{
    QAxObject *sheets = this->querySubObject("Worksheets");
    return sheets->property("Count").toInt();
}

void xlsxWorkbook::addWorksheet(QString name)
{
    QAxObject *worksheets = this->querySubObject("Worksheets");
    QAxObject *sheet = worksheets->querySubObject("Add()");
    sheet->setProperty("Name", name);
}

void xlsxWorkbook::close(bool save)
{
    this->dynamicCall("Close(QVariant)", save);
    delete this;
}

void xlsxWorkbook::refreshAll()
{
    this->dynamicCall("RefreshAll()");
}

void xlsxWorkbook::setDisplayAlerts(bool alerts)
{
    this->querySubObject("Application")->setProperty("DisplayAlerts", alerts);
}

xlsxPivotTables *xlsxWorkbook::getFirstPivotTables()
{
    qDebug() << "K1";
    if (!this) return nullptr;
    qDebug() << "K1/2";
    int sheetsCount = this->getSheetsCount();
    qDebug() << "K2";
    //в приоритете ищу рабочий лист, где есть пивот
    for (int i = 1; i <= sheetsCount; i++)
    {
         xlsxPivotTables *pivotTables = this->openSheet(i)->getPivotTables();
         if (!pivotTables) return nullptr;
         if (pivotTables->getCount() > 0)
             return pivotTables;
         delete pivotTables;
    }
    qDebug() << "K3";
    //если ничего не нашел, тогда возвращаю объект PivotTables с первого листа, даже если там нет пивота
    return this->openSheet(1)->getPivotTables();
}

xlsxConnections *xlsxWorkbook::getConnections()
{
    return (xlsxConnections*) this->querySubObject("Connections()");
}

bool xlsxWorkbook::isWorksheetExist(QString name)
{
    QAxObject *worksheets = this->querySubObject("Worksheets");
    int worksheetsCount = worksheets->property("Count").toInt();
    qDebug() << "COUNT:" << worksheetsCount;
    for (int i = 1; i <= worksheetsCount; i++)
    {
        if (name == worksheets->querySubObject("Item(int)", i)->property("Name"))
        {
            delete worksheets;
            return true;
        }
    }
    delete worksheets;
    return false;
}

