#ifndef XLSXWORKBOOK_H
#define XLSXWORKBOOK_H

#include <QObject>
#include <QAxObject>
#include "xlsxworksheet.h"

class xlsxConnection : public QAxObject
{
public:
    xlsxConnection();
    QString getConnectionString(){return this->property("Connection").toString();}
    QString getCommandString(){return this->property("CommandText").toString();}
    void setConnectionString(QString connectionString){this->setProperty("Connection",connectionString);}
    void setCommandString(QString commandString){this->setProperty("CommandText", commandString);}
    void setCommandType(int sql = 2){this->setProperty("CommandType", sql);}

};

class xlsxConnections : public QAxObject
{
public:
    xlsxConnections();
    int getCount(){return this->property("Count").toInt();}
    xlsxConnection *getConnection(int index){return (xlsxConnection*)this->querySubObject("Item(int)", index)->querySubObject("OLEDBConnection");}
};

class xlsxWorkbook : public QAxObject
{
public:
    xlsxWorkbook();

    xlsxWorksheet *openSheet(int index);
    xlsxWorksheet *openSheet(QString sheetName);

    int getSheetsCount();

    bool isWorksheetExist(QString name);

    void addWorksheet(QString name);
    void close(bool save = false);

    void refreshAll();
    void setDisplayAlerts(bool alerts = "True");

    xlsxPivotTables* getFirstPivotTables();

    xlsxConnections* getConnections();
};

#endif // XLSXWORKBOOK_H
