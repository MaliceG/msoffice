#include "xlsxworksheet.h"
#include <QDebug>
#include <QTextEdit>

xlsxWorksheet::xlsxWorksheet()
{

}

void xlsxWorksheet::setData(int startRow, int startCol, int endRow, int endCol, QVariant var)
{
//    eventCatcher *ec = new eventCatcher();
//    connect(this, SIGNAL(Change(IDispatch*)), ec, SLOT(SheetActivate()));
//    connect(this, SIGNAL(SelectionChange(IDispatch*)), ec, SLOT(newWorkbook()));
//    connect(this, SIGNAL(Calculate()), ec, SLOT(WindowActivate()));

    QAxObject *startCell = this->querySubObject("Cells(int, int)", startRow, startCol);
    QAxObject *endCell = this->querySubObject("Cells(int, int)", endRow, endCol);
    QAxObject *range =  this->querySubObject("Range(const QVariant&, const QVariant&)", startCell->asVariant(), endCell->asVariant());

    range->dynamicCall("SetValue(const QVariant&)", var);
}

void xlsxWorksheet::pasteSpecial(int startRow, int startCol, int endRow, int endCol)
{
    QAxObject *startCell = this->querySubObject("Cells(int, int)", startRow, startCol);
    QAxObject *endCell = this->querySubObject("Cells(int, int)", endRow, endCol);
    QAxObject *range = this->querySubObject("Range(const QVariant&, const QVaritant&)", startCell->asVariant(), endCell->asVariant());
    range->dynamicCall("PasteSpecial (xlPasteValues)");
    startCell->deleteLater();
    endCell->deleteLater();
    range->deleteLater();
}

void xlsxWorksheet::paste()
{
    this->dynamicCall("Paste()");
}

int xlsxWorksheet::getRowCount()
{
    QAxObject *usedRange = this->querySubObject("UsedRange");
    QAxObject *rows = usedRange->querySubObject("Rows");
    int result = rows->property("Count").toInt();

    usedRange->deleteLater();
    rows->deleteLater();

    return result;
}

int xlsxWorksheet::getColCount()
{
    QAxObject *usedRange = this->querySubObject("UsedRange");
    QAxObject *columns = usedRange->querySubObject("Columns");
    int result = columns->property("Count").toInt();

    usedRange->deleteLater();
    columns->deleteLater();

    return result;
}

QVariantList xlsxWorksheet::getRange(int startRow, int startCol, int endRow, int endCol)
{
    QAxObject *startCell = this->querySubObject("Cells(int, int)", startRow, startCol);
    QAxObject *endCell = this->querySubObject("Cells(int, int)", endRow, endCol);
    QAxObject *range =  this->querySubObject("Range(const QVariant&, const QVariant&)", startCell->asVariant(), endCell->asVariant());
    startCell->deleteLater();
    endCell->deleteLater();

    return qvariant_cast<QVariantList> (range->dynamicCall("Value()"));
}

QVariant xlsxWorksheet::getCell(int row, int col)
{
    QAxObject *cell = this->querySubObject("Cells(int, int)", row, col);
    return cell->property("Value");
}

void xlsxWorksheet::setCell(int row, int col, QString value)
{
    QAxObject *cell = this->querySubObject("Cells(int, int)", row, col);
    cell->setProperty("Value", value);
}

void xlsxWorksheet::initPivotTables()
{
    pivotTables = (xlsxPivotTables*) this->querySubObject("PivotTables");
//    qDebug() << pivotTables;

//    return pivotTables;
    //    qDebug() <<  pivotTables;
}

xlsxPivotTables *xlsxWorksheet::getPivotTables()
{
    if (!this) return nullptr;
    QAxObject *object = this->querySubObject("PivotTables");
    if (object) return (xlsxPivotTables*) object;
    else return nullptr;
}

void xlsxWorksheet::selectRange(int startRow, int startColumn, int endRow, int endColumn)
{
    QAxObject *startCell = this->querySubObject("Cells(int, int)", startRow, startColumn);
    QAxObject *endCell = this->querySubObject("Cells(int, int)", endRow, endColumn);
    QAxObject *range =  this->querySubObject("Range(const QVariant&, const QVariant&)", startCell->asVariant(), endCell->asVariant());
    range->dynamicCall("Select");
}

void xlsxWorksheet::copySelection()
{
    QAxObject *app = this->querySubObject("Application");
    QAxObject *selection = app->querySubObject("Selection"); //Selection принадлежит объекту Application
    selection->dynamicCall("Copy");
}

QString xlsxWorksheet::getName()
{
    return this->property("Name").toString();
}

void xlsxWorksheet::setSourceData()
{
    //    this->querySubObject("ActiveChart");
}

void xlsxWorksheet::setRangeFormat(int startRow, int startColumn, int endRow, int endColumn, QString format)
{
    QAxObject *startCell = this->querySubObject("Cells(int, int)", startRow, startColumn);
    QAxObject *endCell = this->querySubObject("Cells(int, int)", endRow, endColumn);
    QAxObject *range =  this->querySubObject("Range(const QVariant&, const QVariant&)", startCell->asVariant(), endCell->asVariant());
    range->setProperty("NumberFormat", format);
    qDebug() << "RANGE FORMAT" << range->dynamicCall("Address").toString();
}

void xlsxWorksheet::clearList()
{
    this->querySubObject("Cells")->dynamicCall("Clear()");
}

void xlsxWorksheet::clearContents()
{
    this->querySubObject("Cells")->dynamicCall("ClearContents()");
}

void xlsxWorksheet::setBordersAll(int startRow, int startColumn, int endRow, int endColumn)
{
    QAxObject *startCell = this->querySubObject("Cells(int, int)", startRow, startColumn);
    QAxObject *endCell = this->querySubObject("Cells(int, int)", endRow, endColumn);
    QAxObject *range =  this->querySubObject("Range(const QVariant&, const QVariant&)", startCell->asVariant(), endCell->asVariant());
    QAxObject *border = range->querySubObject("Borders");
    border->setProperty("LineStyle", 1);
//    border->setProperty("LineStyle(XlLineStyle)", 1);
//    border->setProperty("LineStyle(XlLineStyle)", "xlContinuous");
    //    border->setProperty("Width", 2);
}

void xlsxWorksheet::setRangeColor(int startRow, int startColumn, int endRow, int endColumn, QColor color)
{
    QAxObject *startCell = this->querySubObject("Cells(int, int)", startRow, startColumn);
    QAxObject *endCell = this->querySubObject("Cells(int, int)", endRow, endColumn);
    QAxObject *range =  this->querySubObject("Range(const QVariant&, const QVariant&)", startCell->asVariant(), endCell->asVariant());
    range->querySubObject("Interior")->setProperty("Color", color);
}

void xlsxWorksheet::mergeRange(int startRow, int startColumn, int endRow, int endColumn)
{
    QAxObject *startCell = this->querySubObject("Cells(int, int)", startRow, startColumn);
    QAxObject *endCell = this->querySubObject("Cells(int, int)", endRow, endColumn);
    QAxObject *range =  this->querySubObject("Range(const QVariant&, const QVariant&)", startCell->asVariant(), endCell->asVariant());
    range->dynamicCall("Merge()");
}

void xlsxWorksheet::setRangeAllignment(int startRow, int startColumn, int endRow, int endColumn, xlsxWorksheet::XlAllignment allignment)
{
    QAxObject *startCell = this->querySubObject("Cells(int, int)", startRow, startColumn);
    QAxObject *endCell = this->querySubObject("Cells(int, int)", endRow, endColumn);
    QAxObject *range =  this->querySubObject("Range(const QVariant&, const QVariant&)", startCell->asVariant(), endCell->asVariant());
    range->setProperty("HorizontalAlignment", allignment);
    range->setProperty("VerticalAlignment", allignment);
}

void xlsxWorksheet::setRangeFontColor(int startRow, int startColumn, int endRow, int endColumn, xlsxWorksheet::XlThemeColor color)
{
    QAxObject *startCell = this->querySubObject("Cells(int, int)", startRow, startColumn);
    QAxObject *endCell = this->querySubObject("Cells(int, int)", endRow, endColumn);
    QAxObject *range =  this->querySubObject("Range(const QVariant&, const QVariant&)", startCell->asVariant(), endCell->asVariant());
    range->querySubObject("Font")->setProperty("ThemeColor", color);
    range->querySubObject("Font")->setProperty("TintAndShade", 0);
}
