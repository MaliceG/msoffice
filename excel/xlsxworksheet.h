#ifndef XLSXWORKSHEET_H
#define XLSXWORKSHEET_H

#include <QObject>
#include <QAxObject>
#include "xlsxpivottables.h"

class xlsxWorksheet : public QAxObject
{
public:
    enum XlThemeColor
    {
        xlThemeColorDark1 = 1
    };

    enum XlAllignment
    {
        xlCenter = -4108
    };

    xlsxWorksheet();

    int getRowCount();
    int getColCount();

    QVariantList getRange(int startRow, int startCol, int endRow, int endCol);
    QVariant getCell(int row, int col);

    QString getName();

    xlsxPivotTables *pivotTables; // better not to use this :/

    void setData(int startRow, int startCol, int endRow, int endCol, QVariant var);
    void setCell(int row, int col, QString value);
    void initPivotTables();
    xlsxPivotTables *getPivotTables();
    void selectRange(int startRow, int startColumn, int endRow, int endColumn);
    void copySelection();
    void setSourceData();
    void setRangeFormat(int startRow, int startColumn, int endRow, int endColumn, QString format);
    void clearList();
    void clearContents();
    void setBordersAll(int startRow, int startColumn, int endRow, int endColumn);
    void setRangeFontColor(int startRow, int startColumn, int endRow, int endColumn, XlThemeColor color);
    void setRangeColor(int startRow, int startColumn, int endRow, int endColumn, QColor color);
    void mergeRange(int startRow, int startColumn, int endRow, int endColumn);
    void setRangeAllignment(int startRow, int startColumn, int endRow, int endColumn, XlAllignment allignment);
    void pasteSpecial(int startRow, int startCol, int endRow, int endCol);
    void paste();
private:

};

#endif // XLSXWORKSHEET_H
