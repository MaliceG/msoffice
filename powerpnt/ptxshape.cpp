#include "ptxshape.h"
#include <QDebug>
#include <QTextEdit>
#include <QEventLoop>
#include <QTime>

ptxShape::ptxShape()
{

}

bool ptxShape::isChart()
{
    if (this->dynamicCall("Type").toInt() == 3)
        return true;
    else
        return false;
}

void ptxShape::select()
{
    this->dynamicCall("Select()");
}

void ptxShape::copy()
{
    this->dynamicCall("Copy()");
}

void ptxShape::closeWorkbook(xlsxWorkbook* workbook)
{
    //TODO: разобраться с workbookApp - на текущий момент он нужен для инициализации Excel из PowerPoint
    QAxObject *workbookApp = workbook->querySubObject("Application");

    workbook->dynamicCall("Close()");

    delete workbook;
}

xlsxWorkbook* ptxShape::getWorkbook(bool visible)
{
    QAxObject *chart = this->querySubObject("Chart");
    QAxObject *chartData = chart->querySubObject("ChartData");
    chartData->dynamicCall("Activate()");
    xlsxWorkbook *workbook = (xlsxWorkbook*) chartData->querySubObject("Workbook");
    if (!workbook) return nullptr;
    QAxObject *workbookApp = workbook->querySubObject("Application");
    if (!workbookApp) return nullptr;
    workbookApp->setProperty("Visible", visible);

    workbook->dynamicCall("Activate()");
//    workbook->querySubObject("Windows(1)")->setProperty("Visible", true);
    qDebug() << "Workbook ptr = " << workbook;
    return workbook;
}

xlsxWorksheet* ptxShape::openWorksheet(xlsxWorkbook *workbook, int index)
{
    xlsxWorksheet *wbSheet = (xlsxWorksheet*) workbook->openSheet(index);
    return wbSheet;
}

QString ptxShape::getName()
{
    return this->property("Name").toString();
}

void ptxShape::setName(QString name)
{
    this->setProperty("Name", name);
}

void ptxShape::setSourceData(xlsxWorksheet* wbSheet, int startRow, int startCol, int endRow, int endCol)
{
    QAxObject *startCell = wbSheet->querySubObject("Cells(int, int)", startRow, startCol);
    QAxObject *endCell = wbSheet->querySubObject("Cells(int, int)", endRow, endCol);
    QAxObject *range =  wbSheet->querySubObject("Range(const QVariant&, const QVariant&)", startCell->asVariant(), endCell->asVariant());
    QString sheetName = wbSheet->property("Name").toString();
    QString address = range->dynamicCall("Address").toString();

    qDebug() << QString("='%1'!%2").arg(sheetName).arg(address);
    this->querySubObject("Chart")->dynamicCall("SetSourceData(QString)", QString("='%1'!%2").arg(sheetName).arg(address));
}

QPoint ptxShape::getPos()
{
    QPoint point;
    point.setX(this->property("Left").toInt());
    qDebug() << "LEFT: " << this->property("Left").toInt();
    point.setY(this->property("Top").toInt());
    qDebug() << "TOP: " << this->property("Top").toInt();
    qDebug() << point;
    return point;
}

void ptxShape::setPos(QPoint pos)
{
    qDebug() << "POINTS: " << pos.x() << pos.y();
    this->setProperty("Left", pos.x());
    this->setProperty("Top", pos.y());
}

int ptxShape::getWidht()
{
    return this->property("Width").toInt();
}

int ptxShape::getHeight()
{
    return this->property("Height").toInt();
}

void ptxShape::setWidth(int width)
{
    this->setProperty("Width", width);
}

void ptxShape::remove()
{
    this->dynamicCall("Delete()");
}

QString ptxShape::getText()
{
    QAxObject *txtFrame = this->querySubObject("TextFrame");
    if (txtFrame->dynamicCall("HasText()").toBool())
        return txtFrame->querySubObject("TextRange")->property("Text").toString();
    return QString();
}

void ptxShape::setText(QString text)
{
    QAxObject *txtFrame = this->querySubObject("TextFrame");
    if (txtFrame)
        txtFrame->querySubObject("TextRange")->setProperty("Text", text);
}

int ptxShape::getType()
{
    return this->dynamicCall("Type").toInt() == 3;
}

int ptxShape::getChartType()
{
    QAxObject *chart = this->querySubObject("Chart");
    return chart->dynamicCall("ChartType").toInt();
}

int ptxShape::getID()
{
    return this->dynamicCall("Id()").toInt();
}

void ptxShape::test()
{
    qDebug() << "CONNECTED";
}

