#ifndef PTXSHAPE_H
#define PTXSHAPE_H

#include <QObject>
#include <QAxObject>
#include "excel\xlsxworksheet.h"
#include "excel\xlsxworkbook.h"
#include <shlobj.h>

class ptxShape : public QAxObject
{
    Q_OBJECT
private:
//    QAxObject *chart;
//    QAxObject *chartData;
//    QAxObject *workbook;
//    QAxObject *workbookApp;

public:
    ptxShape();

    void closeWorkbook(xlsxWorkbook *workbook);
    void setSourceData(xlsxWorksheet *wbSheet, int startRow, int startCol, int endRow, int endCol);
    void setWidth(int width);
    void setPos(QPoint pos);
    void remove();
    void setText(QString text);

    xlsxWorkbook* getWorkbook(bool visible = false);
    xlsxWorksheet* openWorksheet(xlsxWorkbook *workbook, int index);

    QString getName();
    void setName(QString name);
    QString getText();
    QPoint getPos();

    int getHeight();
    int getWidht();
    int getType();
    int getChartType();
    int getID();

    bool isChart();
    void select();

    void copy();

//    xlsxWorksheet *wbSheet;

public slots:
    void test();
};

#endif // PTXSHAPE_H
