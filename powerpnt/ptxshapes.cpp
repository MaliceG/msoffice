#include "ptxshapes.h"
#include <QDebug>
#include <QTextEdit>

ptxshapes::ptxshapes()
{

}

int ptxshapes::getCount()
{
    return this->property("Count").toInt();
}

ptxShape *ptxshapes::getShape(int index)
{
    return (ptxShape*)this->querySubObject("item(int)", index);
}

ptxShape *ptxshapes::getShape(QString name)
{
    return (ptxShape*)this->querySubObject("item(QString)", name);
}

ptxShape *ptxshapes::getShape(QString name, int ID)
{
    ptxShape *wantedShape = nullptr;
    for (int i = 1; i <= this->getCount(); i++)
    {
        ptxShape *shape = this->getShape(i);
        if (shape->getName() == name && shape->getID() == ID)
        {
            wantedShape = shape;
            break;
        }
        delete shape;
    }

    return wantedShape;
}

ptxShape *ptxshapes::addChart2(ptxshapes::chartType type, int x1, int y1, int x2, int y2)
{
    qDebug() << "Return ptxShape";
    return (ptxShape*)this->querySubObject("AddChart2(int, Office::XlChartType , int, int, int, int)", -1, type, x1, y1, x2, y2);
}

ptxShape *ptxshapes::addChart(ptxshapes::chartType type, int x1, int y1, int x2, int y2)
{
    return (ptxShape*) this->querySubObject("AddChart(Office::XlChartType , int, int, int, int)", type, x1, y1, x2, y2);
}

QStringList ptxshapes::getAllShapes()
{
    QStringList shapesList;
    for (int i = 1; i <= this->getCount(); i++)
        shapesList.append(this->getShape(i)->getName());

    return shapesList;
}

void ptxshapes::pasteWithFormat()
{
    this->dynamicCall("PasteSpecial(ppPasteEnhancedMetafile)");
}

void ptxshapes::paste()
{
    this->dynamicCall("Paste()");
}

void ptxshapes::deleteAllShapes()
{
    for (int i = 1; i <= this->getCount(); i++)
    {
        qDebug() << this->getShape(i)->getName();
        this->getShape(i)->remove();
    }
}

