#ifndef PTXSHAPES_H
#define PTXSHAPES_H

#include <QObject>
#include <QAxObject>
#include "ptxshape.h"

class ptxshapes : public QAxObject
{

public:
    enum chartType
    {
        xlLine = 4,
        xlPie = 5
    };

    ptxshapes();

    int getCount();

    ptxShape *getShape(int index);
    ptxShape *getShape(QString name);
    ptxShape *getShape(QString name, int ID);

    ptxShape *addChart2(chartType type, int x1, int y1, int x2, int y2);
    ptxShape *addChart(chartType type, int x1, int y1, int x2, int y2);

    QStringList getAllShapes();

    void pasteWithFormat();
    void paste();

    void deleteAllShapes();

};

#endif // PTXSHAPES_H
