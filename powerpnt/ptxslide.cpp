#include "ptxslide.h"
#include <QDebug>

ptxSlide::ptxSlide()
{
    qDebug() << "ptxSlide consttructor";
}

ptxshapes *ptxSlide::shapes()
{
    QAxObject *object = this->querySubObject("Shapes");
    int tries = 5;
    while (tries > 0)
    {
        object = this->querySubObject("Shapes");
        if (!object)
            tries--;
        else
            break;
        Sleep(200);
        qDebug() << "Не удалось получить Object Shapes";
    }

    if (!object)
        return NULL;

    return (ptxshapes*)object;
}

QString ptxSlide::getName()
{
    return this->property("Name").toString();
}

void ptxSlide::setName(QString slideName)
{
    this->setProperty("Name", slideName);
}

void ptxSlide::select()
{
    this->dynamicCall("Select()");
}

void ptxSlide::copy()
{
    this->dynamicCall("Copy()");
}

void ptxSlide::deleteCurrentSlide()
{
    this->dynamicCall("Delete()");
}

bool ptxSlide::hasCharts()
{
    for (int i = 1; i <= this->shapes()->getCount(); i++)
        if (this->shapes()->getShape(i)->isChart())
            return true;
    return false;
}

