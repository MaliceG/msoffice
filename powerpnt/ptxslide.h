#ifndef PTXSLIDE_H
#define PTXSLIDE_H
#include <QAxObject>
#include <QObject>
#include "ptxshapes.h"

class ptxSlide : public QAxObject
{
public:
    ptxSlide();
    ptxshapes *shapes();
    QString getName();
    void setName(QString slideName);
    void select();
    void copy();
    void deleteCurrentSlide();
    bool hasCharts();
};

#endif // SLIDE_H
