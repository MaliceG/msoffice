#include "ptxslides.h"
#include <QDebug>

ptxSlides::ptxSlides()
{

}

int ptxSlides::getCount()
{
    return this->property("Count").toInt();
}

ptxSlide *ptxSlides::paste(int slideIndex)
{
    qDebug() << this->querySubObject("Paste(int)", slideIndex);
    return this->getSlide(slideIndex);
}

ptxSlide *ptxSlides::pasteToTheEnd()
{
    this->dynamicCall("Paste(int)", this->getCount() + 1);
    return getSlide(this->getCount());
}

ptxSlide *ptxSlides::getSlide(int index)
{
    QAxObject *object = this->querySubObject("Item(int)", index);
    int tries = 5;
    while (tries > 0)
    {
        object = this->querySubObject("Item(int)", index);
        if (!object)
            tries--;
        else
            break;
        Sleep(200);
        qDebug() << "Не удалось получить Object Slide";
    }

    if (!object)
        return NULL;

    return (ptxSlide*)object;
}

ptxSlide *ptxSlides::addSlide(int index)
{
    return (ptxSlide*) this->querySubObject("Add(int, PpSlideLayout)", index, "ppLayoutCustom");
}

ptxShape *ptxSlides::findShape(QString name)
{
    for (int i = 1; i <= this->getCount(); i++)
    {
        ptxSlide *slide = this->getSlide(i);
        for (int j = 1; j <= slide->shapes()->getCount(); j++)
        {
            ptxShape *shape = slide->shapes()->getShape(j);
            qDebug() << name << shape->getName();
            if (name == shape->getName())
                return shape;
        }
    }

    return nullptr;
}

int ptxSlides::getChartsCount()
{
    int chartsCount = 0;
    for (int i = 1; i <= this->getCount(); i++)
    {
        ptxSlide *slide = this->getSlide(i);
        for (int j = 1; j <= slide->shapes()->getCount(); j++)
        {
            if (slide->shapes()->getShape(j)->isChart())
                chartsCount++;
        }
        delete slide;
    }
    return chartsCount;
}

