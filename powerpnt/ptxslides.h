#ifndef PTXSLIDES_H
#define PTXSLIDES_H
#include <QAxObject>
#include "ptxslide.h"
#include <QObject>


class ptxSlides : public QAxObject
{   
    Q_OBJECT
public:
    explicit ptxSlides();

    ptxSlide *getSlide(int index);
    ptxSlide *addSlide(int index);
    ptxShape *findShape(QString name);
    int getChartsCount();

    int getCount();
    ptxSlide *paste(int slideIndex);
    ptxSlide *pasteToTheEnd();

};

#endif // SLIDES_H
