#include "qtpptx.h"
#include <QDebug>

QtPptx::QtPptx()
{
    CoInitializeEx(NULL, COINIT_MULTITHREADED);
    powerPoint = new QAxObject("PowerPoint.Application", 0);
    presentations = powerPoint->querySubObject("Presentations");
    presentation = NULL;
}

bool QtPptx::openPptx(QString path, bool visible)
{
    //visible не распространяется на новые презентации (newPresentation();)
    QVariantList params = {path.replace("/","\\\\"), false, false, visible};

    presentation = presentations->querySubObject("Open(QString, Office::MsoTriState, Office::MsoTriState, Office::MsoTriState)", params);
    if (!presentation)
    {
        params[0] = path;
        presentation = presentations->querySubObject("Open(QString, Office::MsoTriState, Office::MsoTriState, Office::MsoTriState)", params);
    }

    if (!presentation) //проверяем, сумела ли презентация открыться во второй раз
        return false;

    slides = (ptxSlides *)presentation->querySubObject("Slides");
    return true;
}

void QtPptx::setVisbility(bool visible)
{
    powerPoint->setProperty("Visible", visible);
}

void QtPptx::closeAll()
{
    presentations->deleteLater();
    presentation->dynamicCall("Close()");
    presentation->deleteLater();
    powerPoint->dynamicCall("Quit()");
    powerPoint->deleteLater();
}

void QtPptx::closePresentation()
{
    presentation->dynamicCall("Close()");
}

void QtPptx::saveAs(QString path, QString name)
{
    presentation->dynamicCall("saveAs(QString)", path.replace("/", "\\") + "\\" + name + ".pptx");
}

void QtPptx::saveAs(QString fullPath)
{
     presentation->dynamicCall("saveAs(QString)", fullPath.replace("/", "\\"));
}

void QtPptx::save()
{
    presentation->dynamicCall("Save()");
}

ptxSlides *QtPptx::newPresentation()
{
    presentation = presentations->querySubObject("Add()");
    slides = (ptxSlides *)presentation->querySubObject("Slides");
    return slides;
}

ptxSlides *QtPptx::getSlides()
{
    return slides;
}

QString QtPptx::getPresentationComments()
{
    return presentation->querySubObject("BuiltInDocumentProperties")->querySubObject("Item(\"comments\")")->dynamicCall("Value").toString();
}

QString QtPptx::getName()
{
    if (presentation) return presentation->property("Name").toString();
    else return QString();
}

QString QtPptx::getFullPath()
{
    if (presentation)
    {
        QString filePath = presentation->property("Path").toString() + "\\" + getName();
        if (!filePath.contains(".pptx")) filePath += ".pptx";
        return  filePath;
    }
    return QString();
}

void QtPptx::setPresentationComments(QString comment)
{
    presentation->querySubObject("BuiltInDocumentProperties")->querySubObject("Item(\"comments\")")->dynamicCall("SetValue(QString)", comment);
}

QPoint QtPptx::getSize()
{
    if (presentation)
    {
        int slideWidth = presentation->querySubObject("PageSetup")->property("SlideWidth").toInt();
        int slideHeight = presentation->querySubObject("PageSetup")->property("SlideHeight").toInt();
        return QPoint(slideWidth, slideHeight);
    }

    return QPoint(0,0);
}

