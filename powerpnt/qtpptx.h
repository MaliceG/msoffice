#ifndef QTPPTX_H
#define QTPPTX_H

#include <QObject>
#include <QAxObject>
#include <shlobj.h>
#include "ptxslides.h"

class QtPptx : public QObject
{
    Q_OBJECT
public:
    QtPptx();
    bool openPptx(QString path, bool visible = true);
    void setVisbility(bool visible);
    void closeAll();
    void closePresentation();
    void saveAs(QString path, QString name);
    void saveAs(QString fullPath);
    void save();
    ptxSlides *newPresentation();
    ptxSlides *getSlides();

    ptxSlides *slides = NULL;
    QAxObject *presentation = NULL;

    QString getPresentationComments();
    QString getName();
    QString getFullPath();
    void setPresentationComments(QString comment);

    QPoint getSize();

private:
    QAxObject *powerPoint;
    QAxObject *presentations;
};

#endif // QTPPTX_H
